# GemCrush - Bird Crush
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9c54d277d4d84a4d8dd0f98b1269b767)](https://www.codacy.com/manual/kennyyuen2008/GemCrush_2?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=kennyyuen2008/GemCrush&amp;utm_campaign=Badge_Grade)
## Game Introduction and Playing method:
There are 7 different color of gems on the 8x8 board. User can swap between any two of the adjacent gems by clicking the gems. The gems will be eliminated when three or more gems of the same color are horizontal or vertical adjacent to each other. 10 points will be rewarded to user of every gem they eliminate. After each elimination, new gems will be generated and slide down to fill out all the empty space. Users would be able to preview the next two gems that will generate. Moreover, users can see their total marks after the 5 minuets time limit. They can compare the highest scores with others and save or load game. 

## Features:
* Displaying all time highest marks. User can compare their scores with the highest scores made by him or his friends. 
* The highest score will be automatically saved when user break the old record. 
* Different interesting sound effect, allow user to have more fun while playing. 
* Better looking gems. Gems are now bird characters. 
* Three game saving slots. User can save more than one game play. 
* Brighter and different looking background for better visual looking. 

## Build
The Executable jar file is located at dist/GemsCrush.jar. Feel free to try it out.

## Screen Shot
![Screen Shot](/images/ui.png)
